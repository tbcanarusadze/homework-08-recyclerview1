package com.example.recycleview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_recycleview_layout.*

class MainActivity : AppCompatActivity() {
    private val items = mutableListOf<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        adapter = RecyclerViewAdapter(items)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        setData()

    }
    private fun setData(){
        items.add(ItemModel(R.mipmap.pleasepleaseme, "Please Please Me", "1963"))
        items.add(ItemModel(R.mipmap.withthebeatlescover, "With The Beatles", "1963"))
        items.add(ItemModel(R.mipmap.aharddaysnight, "A Hard Day's Night", "1964"))
        items.add(ItemModel(R.mipmap.beatlesforsale, "Beatles for Sale", "1964"))
        items.add(ItemModel(R.mipmap.help, "Help!", "1965"))
        items.add(ItemModel(R.mipmap.rubbersoul, "Rubber Soul", "1965"))
        items.add(ItemModel(R.mipmap.revolver, "Revolver", "1966"))
        items.add(ItemModel(R.mipmap.sgtpepper, "Sgt. Peppers Lonely Hearts Club Band", "1967"))
        items.add(ItemModel(R.mipmap.magicaltour, "Magical Mystery Tour", "1967"))
        items.add(ItemModel(R.mipmap.white, "The White Album", "1968"))
        items.add(ItemModel(R.mipmap.yellowsubmarine, "Yellow Submarine", "1969"))
        items.add(ItemModel(R.mipmap.abbeyroad, "Abbey Road", "1969"))
        items.add(ItemModel(R.mipmap.letitbe, "Let It Be", "1970"))
    }

}
